﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace InstaFeed
{
    public class Loader : Control
    {
        private Timer tmr;
        private int step = 0;
        private int numSteps = 8;
        private int imgWidth = 128;

        protected override void OnVisibleChanged(EventArgs e)
        {
            base.OnVisibleChanged(e);
            this.step = 0;
        }

        public Loader()
        {
            tmr = new Timer();
            SetStyle(ControlStyles.SupportsTransparentBackColor | ControlStyles.OptimizedDoubleBuffer, true);
            this.BackColor = Color.Transparent;
            this.Size = new Size(64, 64);

            tmr.Tick += tmr_Tick;
            tmr.Enabled = true;
        }

        private void tmr_Tick(object sender, EventArgs e)
        {
            if (!DesignMode)
            {
                step++;
                if (step >= numSteps)
                    step = 0;
                this.Invalidate();
            }
        }

        public int Rate
        {
            get
            {
                return tmr.Interval;
            }
            set
            {
                tmr.Interval = value;
            }
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);
            e.Graphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            if (Parent != null)
            {
                Bitmap behind = new Bitmap(Parent.Width, Parent.Height);
                foreach (Control c in Parent.Controls)
                    if (c.Bounds.IntersectsWith(this.Bounds) & c != this)
                        c.DrawToBitmap(behind, c.Bounds);
                e.Graphics.DrawImage(behind, -Left, -Top);
                behind.Dispose();
            }
            e.Graphics.DrawImage(InstaFeed.Properties.Resources.loader, new Rectangle(0, 0, this.Width, this.Height), step * imgWidth, 0, imgWidth, imgWidth, GraphicsUnit.Pixel);
        }
    }
}
