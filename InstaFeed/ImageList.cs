﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;
using System.Net;

namespace InstaFeed
{
    public class ImageList : IDisposable
    {
        private class taginfo
        {
            public string tag;
            public DateTime from;
            public DateTime to;
            public int max;
        }

        System.Windows.Forms.Timer delayTimer;
        private BackgroundWorker wkLoadImage;
        private BackgroundWorker wkLoadImages;
        private taginfo currentInfo;
        private Instagram instagram;
        public List<InstagramImage> images;
        public int currentIndex;
        public Bitmap current;
        public Bitmap next;
        public bool Blackout = false;

        public event EventHandler ReloadListBegin = new EventHandler((object o, EventArgs e) => { });
        public event EventHandler ReloadListComplete = new EventHandler((object o, EventArgs e) => { });

        public event EventHandler LoadNextImageBegin = new EventHandler((object o, EventArgs e) => { });
        public event EventHandler LoadNextImageComplete = new EventHandler((object o, EventArgs e) => { });

        public event EventHandler CurrentChanged = new EventHandler((object o, EventArgs e) => { });

        public ImageList()
        {
            instagram = new Instagram("663cf2d1191c41cd965cfa11fac72c6b");
            wkLoadImage = new BackgroundWorker();
            wkLoadImages = new BackgroundWorker();
            images = new List<InstagramImage>();
            delayTimer = new System.Windows.Forms.Timer();
            delayTimer.Interval = 1000;
            delayTimer.Tick += delayTimer_Tick;

            wkLoadImage.DoWork += wkLoadImage_DoWork;
            wkLoadImage.RunWorkerCompleted += wkLoadImage_RunWorkerCompleted;

            wkLoadImages.DoWork += wkLoadImages_DoWork;
            wkLoadImages.RunWorkerCompleted += wkLoadImages_RunWorkerCompleted;
        }

        void delayTimer_Tick(object sender, EventArgs e)
        {
            if (!wkLoadImage.IsBusy)
            {
                delayTimer.Stop();
                Advance();
            }
        }

        void wkLoadImages_DoWork(object sender, DoWorkEventArgs e)
        {
            taginfo t = (taginfo)e.Argument;
            try
            {
                e.Result = instagram.GetImages(t.tag,t.from,t.to,t.max);
            }
            catch
            {
                
            }
        }

        void wkLoadImages_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            images.Clear();
            if (e.Result != null)
            {
                images.AddRange((InstagramImage[])e.Result);
            }
            ReloadListComplete(this, new EventArgs());
        }

        void wkLoadImage_DoWork(object sender, DoWorkEventArgs e)
        {
            e.Result = DownloadBitmap((string)e.Argument);
        }

        void wkLoadImage_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            next = (Bitmap)e.Result;
            LoadNextImageComplete(this, new EventArgs());
        }

        private Bitmap DownloadBitmap(string url)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            return (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(response.GetResponseStream());
        }

        public int nextIndex()
        {
            if (currentIndex + 1 >= images.Count) return 0;
            return currentIndex + 1;
        }
        public int prevIndex()
        {
            if (currentIndex - 1 < 0) return images.Count - 1;
            return currentIndex - 1;
        }

        public void SetTag(string tag, DateTime from, DateTime to, int max)
        {
            currentInfo = new taginfo();
            currentInfo.tag = tag;
            currentInfo.from = from;
            currentInfo.to = to;
            currentInfo.max = max;
            ReloadListBegin(this, new EventArgs());
            wkLoadImages.RunWorkerAsync(currentInfo);
        }

        public string GetCurrentText()
        {
            if (currentIndex < 0 || currentIndex > images.Count) return "";
            return "@" + images[currentIndex].User + "\n" + images[currentIndex].Message;
        }

        public void Advance()
        {
            if (images.Count == 0) return;
            if (wkLoadImage.IsBusy || next == null)
            {
                delayTimer.Start();
            }
            else
            {
                Bitmap tmp = current;
                if (!Blackout)
                    current = next;
                else
                    current = null;
                next = null;
                currentIndex = nextIndex();
                CurrentChanged(this, new EventArgs());
                LoadNextImageBegin(this, new EventArgs());
                try
                {
                    wkLoadImage.RunWorkerAsync(images[nextIndex()].ImageUrl);
                }
                catch
                {
                }

                if (tmp != null)
                    tmp.Dispose();
                tmp = null;
                if (next != null)
                    next.Dispose();
                next = null;
            }
        }

        public void SetNext(int i)
        {
            if (images.Count == 0 || wkLoadImage.IsBusy) return;
            Bitmap tmp = next;
            next = null;
            LoadNextImageBegin(this, new EventArgs());
            currentIndex = i - 1;
            if (currentIndex >= images.Count)
                currentIndex = 0;
            wkLoadImage.RunWorkerAsync(images[nextIndex()].ImageUrl);
            tmp.Dispose();
        }

        public void BlackOut(bool enable)
        {
            if (enable)
            {
                Bitmap tmp = next;
                next = current;
                currentIndex = prevIndex();

                current = null;
                CurrentChanged(this, new EventArgs());
                LoadNextImageComplete(this, new EventArgs());

                if (tmp != null)
                    tmp.Dispose();
                tmp = null;
                Blackout = true;
            }
            else
            {
                Blackout = false;
                Advance();
            }
        }

        public void Dispose()
        {
            if (current != null)
            {
                current.Dispose();
                current = null;
            }
            if (next != null)
            {
                next.Dispose();
                next = null;
            }
        }
    }
}
