﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;

namespace InstaFeed
{
    public static class Recolor
    {

        public static Bitmap Alpha(Bitmap input, Color colorOut)
        {
            Bitmap res = new Bitmap(input.Size.Width, input.Size.Height);
            ImageAttributes attr = new ImageAttributes();
            ColorMatrix cmAlpha = new ColorMatrix(new float[][]
            {
                new float[] { 0, 0, 0, 0, 0 },
                new float[] { 0, 0, 0, 0, 0 },
                new float[] { 0, 0, 0, 0, 0 },
                new float[] { (float)colorOut.R/255.0f, (float)colorOut.G/255.0f, (float)colorOut.B/255.0f,(float)colorOut.A/255.0f, 1 },
                new float[] { 0, 0, 0, 0, 0 },
            });
            attr.SetColorMatrix(cmAlpha);

            using (Graphics gr = Graphics.FromImage(res))
            {
                gr.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                gr.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                gr.DrawImage(input, new Rectangle(new Point(0, 0), input.Size), 0, 0, input.Width, input.Height, GraphicsUnit.Pixel, attr);
            }

            return res;
        }
    }
}
