﻿namespace InstaFeed
{
    partial class Controller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtTag = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtFrom = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtTo = new System.Windows.Forms.DateTimePicker();
            this.pnlFilter = new System.Windows.Forms.Panel();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.nbMax = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.lvData = new System.Windows.Forms.ListView();
            this.colUser = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.colDate = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.pnlView = new System.Windows.Forms.Panel();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.imgNext = new System.Windows.Forms.PictureBox();
            this.imgCurrent = new System.Windows.Forms.PictureBox();
            this.pnlControl = new System.Windows.Forms.Panel();
            this.chkBlackout = new System.Windows.Forms.CheckBox();
            this.btnSetImage = new System.Windows.Forms.Button();
            this.nbTime = new System.Windows.Forms.NumericUpDown();
            this.btnPrev = new System.Windows.Forms.Button();
            this.btnNext = new System.Windows.Forms.Button();
            this.btnPause = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.pnlData = new System.Windows.Forms.Panel();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblFont = new System.Windows.Forms.Label();
            this.btnFont = new System.Windows.Forms.Button();
            this.btnAbout = new System.Windows.Forms.Button();
            this.ddScreen = new System.Windows.Forms.ComboBox();
            this.chkPresenter = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.tmrPlay = new System.Windows.Forms.Timer(this.components);
            this.chkShowCTag = new System.Windows.Forms.CheckBox();
            this.ldData = new InstaFeed.Loader();
            this.ldPreview = new InstaFeed.Loader();
            this.cnt = new InstaFeed.Countdown();
            this.pnlFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbMax)).BeginInit();
            this.pnlView.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgNext)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurrent)).BeginInit();
            this.pnlControl.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbTime)).BeginInit();
            this.pnlData.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // txtTag
            // 
            this.txtTag.Location = new System.Drawing.Point(3, 16);
            this.txtTag.Name = "txtTag";
            this.txtTag.Size = new System.Drawing.Size(148, 20);
            this.txtTag.TabIndex = 0;
            this.txtTag.TextChanged += new System.EventHandler(this.txtTag_TextChanged);
            this.txtTag.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtTag_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(47, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Hashtag";
            // 
            // dtFrom
            // 
            this.dtFrom.Location = new System.Drawing.Point(3, 55);
            this.dtFrom.Name = "dtFrom";
            this.dtFrom.Size = new System.Drawing.Size(148, 20);
            this.dtFrom.TabIndex = 2;
            this.dtFrom.ValueChanged += new System.EventHandler(this.dtFrom_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Date from";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 78);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Date to";
            // 
            // dtTo
            // 
            this.dtTo.Location = new System.Drawing.Point(3, 94);
            this.dtTo.Name = "dtTo";
            this.dtTo.Size = new System.Drawing.Size(148, 20);
            this.dtTo.TabIndex = 4;
            this.dtTo.ValueChanged += new System.EventHandler(this.dtTo_ValueChanged);
            // 
            // pnlFilter
            // 
            this.pnlFilter.Controls.Add(this.btnRefresh);
            this.pnlFilter.Controls.Add(this.nbMax);
            this.pnlFilter.Controls.Add(this.label1);
            this.pnlFilter.Controls.Add(this.label4);
            this.pnlFilter.Controls.Add(this.txtTag);
            this.pnlFilter.Controls.Add(this.dtFrom);
            this.pnlFilter.Controls.Add(this.label3);
            this.pnlFilter.Controls.Add(this.label2);
            this.pnlFilter.Controls.Add(this.dtTo);
            this.pnlFilter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlFilter.Location = new System.Drawing.Point(3, 3);
            this.pnlFilter.Name = "pnlFilter";
            this.pnlFilter.Size = new System.Drawing.Size(153, 194);
            this.pnlFilter.TabIndex = 6;
            // 
            // btnRefresh
            // 
            this.btnRefresh.BackgroundImage = global::InstaFeed.Properties.Resources.refresh27;
            this.btnRefresh.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnRefresh.Location = new System.Drawing.Point(119, 159);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(32, 32);
            this.btnRefresh.TabIndex = 9;
            this.btnRefresh.UseVisualStyleBackColor = true;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // nbMax
            // 
            this.nbMax.Location = new System.Drawing.Point(3, 133);
            this.nbMax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nbMax.Name = "nbMax";
            this.nbMax.Size = new System.Drawing.Size(148, 20);
            this.nbMax.TabIndex = 8;
            this.nbMax.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nbMax.ValueChanged += new System.EventHandler(this.nbMax_ValueChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(3, 117);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(63, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Max images";
            // 
            // lvData
            // 
            this.lvData.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.colUser,
            this.colDate});
            this.lvData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvData.FullRowSelect = true;
            this.lvData.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.None;
            this.lvData.HideSelection = false;
            this.lvData.Location = new System.Drawing.Point(0, 0);
            this.lvData.Name = "lvData";
            this.lvData.Size = new System.Drawing.Size(622, 827);
            this.lvData.TabIndex = 7;
            this.lvData.UseCompatibleStateImageBehavior = false;
            this.lvData.View = System.Windows.Forms.View.Details;
            this.lvData.SelectedIndexChanged += new System.EventHandler(this.lvData_SelectedIndexChanged);
            // 
            // colUser
            // 
            this.colUser.Text = "User name";
            this.colUser.Width = 123;
            // 
            // colDate
            // 
            this.colDate.Text = "Date";
            this.colDate.Width = 131;
            // 
            // pnlView
            // 
            this.pnlView.Controls.Add(this.splitContainer1);
            this.pnlView.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlView.Location = new System.Drawing.Point(0, 0);
            this.pnlView.Name = "pnlView";
            this.pnlView.Size = new System.Drawing.Size(471, 794);
            this.pnlView.TabIndex = 8;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.ldPreview);
            this.splitContainer1.Panel1.Controls.Add(this.imgNext);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.imgCurrent);
            this.splitContainer1.Size = new System.Drawing.Size(471, 794);
            this.splitContainer1.SplitterDistance = 222;
            this.splitContainer1.TabIndex = 0;
            // 
            // imgNext
            // 
            this.imgNext.BackColor = System.Drawing.Color.Black;
            this.imgNext.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgNext.Location = new System.Drawing.Point(0, 0);
            this.imgNext.Name = "imgNext";
            this.imgNext.Size = new System.Drawing.Size(471, 222);
            this.imgNext.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgNext.TabIndex = 0;
            this.imgNext.TabStop = false;
            // 
            // imgCurrent
            // 
            this.imgCurrent.BackColor = System.Drawing.Color.Black;
            this.imgCurrent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imgCurrent.Location = new System.Drawing.Point(0, 0);
            this.imgCurrent.Name = "imgCurrent";
            this.imgCurrent.Size = new System.Drawing.Size(471, 568);
            this.imgCurrent.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.imgCurrent.TabIndex = 1;
            this.imgCurrent.TabStop = false;
            // 
            // pnlControl
            // 
            this.pnlControl.Controls.Add(this.cnt);
            this.pnlControl.Controls.Add(this.chkBlackout);
            this.pnlControl.Controls.Add(this.btnSetImage);
            this.pnlControl.Controls.Add(this.nbTime);
            this.pnlControl.Controls.Add(this.btnPrev);
            this.pnlControl.Controls.Add(this.btnNext);
            this.pnlControl.Controls.Add(this.btnPause);
            this.pnlControl.Controls.Add(this.btnPlay);
            this.pnlControl.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlControl.Location = new System.Drawing.Point(0, 794);
            this.pnlControl.Name = "pnlControl";
            this.pnlControl.Size = new System.Drawing.Size(471, 39);
            this.pnlControl.TabIndex = 9;
            // 
            // chkBlackout
            // 
            this.chkBlackout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkBlackout.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkBlackout.BackgroundImage = global::InstaFeed.Properties.Resources.black170;
            this.chkBlackout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.chkBlackout.Location = new System.Drawing.Point(41, 3);
            this.chkBlackout.Name = "chkBlackout";
            this.chkBlackout.Size = new System.Drawing.Size(32, 32);
            this.chkBlackout.TabIndex = 7;
            this.chkBlackout.UseVisualStyleBackColor = true;
            this.chkBlackout.CheckedChanged += new System.EventHandler(this.chkBlackout_CheckedChanged);
            // 
            // btnSetImage
            // 
            this.btnSetImage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSetImage.BackgroundImage = global::InstaFeed.Properties.Resources.arrow167;
            this.btnSetImage.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnSetImage.Location = new System.Drawing.Point(3, 3);
            this.btnSetImage.Name = "btnSetImage";
            this.btnSetImage.Size = new System.Drawing.Size(32, 32);
            this.btnSetImage.TabIndex = 5;
            this.btnSetImage.UseVisualStyleBackColor = true;
            this.btnSetImage.Click += new System.EventHandler(this.btnSetImage_Click);
            // 
            // nbTime
            // 
            this.nbTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.nbTime.DecimalPlaces = 1;
            this.nbTime.Location = new System.Drawing.Point(331, 11);
            this.nbTime.Name = "nbTime";
            this.nbTime.Size = new System.Drawing.Size(61, 20);
            this.nbTime.TabIndex = 4;
            this.nbTime.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.nbTime.ValueChanged += new System.EventHandler(this.nbTime_ValueChanged);
            // 
            // btnPrev
            // 
            this.btnPrev.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPrev.BackgroundImage = global::InstaFeed.Properties.Resources.media24;
            this.btnPrev.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPrev.Location = new System.Drawing.Point(217, 3);
            this.btnPrev.Name = "btnPrev";
            this.btnPrev.Size = new System.Drawing.Size(32, 32);
            this.btnPrev.TabIndex = 3;
            this.btnPrev.UseVisualStyleBackColor = true;
            this.btnPrev.Click += new System.EventHandler(this.btnPrev_Click);
            // 
            // btnNext
            // 
            this.btnNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnNext.BackgroundImage = global::InstaFeed.Properties.Resources.forward11;
            this.btnNext.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnNext.Location = new System.Drawing.Point(255, 3);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(32, 32);
            this.btnNext.TabIndex = 2;
            this.btnNext.UseVisualStyleBackColor = true;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // btnPause
            // 
            this.btnPause.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPause.BackgroundImage = global::InstaFeed.Properties.Resources.pause18;
            this.btnPause.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPause.Location = new System.Drawing.Point(398, 3);
            this.btnPause.Name = "btnPause";
            this.btnPause.Size = new System.Drawing.Size(32, 32);
            this.btnPause.TabIndex = 1;
            this.btnPause.UseVisualStyleBackColor = true;
            this.btnPause.Click += new System.EventHandler(this.btnPause_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlay.BackgroundImage = global::InstaFeed.Properties.Resources.media23;
            this.btnPlay.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btnPlay.Location = new System.Drawing.Point(436, 3);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(32, 32);
            this.btnPlay.TabIndex = 0;
            this.btnPlay.UseVisualStyleBackColor = true;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // pnlData
            // 
            this.pnlData.Controls.Add(this.ldData);
            this.pnlData.Controls.Add(this.lvData);
            this.pnlData.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlData.Location = new System.Drawing.Point(168, 3);
            this.pnlData.Name = "pnlData";
            this.pnlData.Size = new System.Drawing.Size(622, 827);
            this.pnlData.TabIndex = 12;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.tableLayoutPanel1);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.panel1);
            this.splitContainer2.Size = new System.Drawing.Size(1263, 833);
            this.splitContainer2.SplitterDistance = 788;
            this.splitContainer2.TabIndex = 13;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.Controls.Add(this.pnlData, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(788, 833);
            this.tableLayoutPanel1.TabIndex = 14;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.pnlFilter, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(159, 827);
            this.tableLayoutPanel2.TabIndex = 13;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.chkShowCTag);
            this.panel2.Controls.Add(this.lblFont);
            this.panel2.Controls.Add(this.btnFont);
            this.panel2.Controls.Add(this.btnAbout);
            this.panel2.Controls.Add(this.ddScreen);
            this.panel2.Controls.Add(this.chkPresenter);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 203);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(153, 194);
            this.panel2.TabIndex = 7;
            // 
            // lblFont
            // 
            this.lblFont.AutoSize = true;
            this.lblFont.Location = new System.Drawing.Point(6, 50);
            this.lblFont.Name = "lblFont";
            this.lblFont.Size = new System.Drawing.Size(28, 13);
            this.lblFont.TabIndex = 4;
            this.lblFont.Text = "Font";
            // 
            // btnFont
            // 
            this.btnFont.Location = new System.Drawing.Point(3, 66);
            this.btnFont.Name = "btnFont";
            this.btnFont.Size = new System.Drawing.Size(147, 23);
            this.btnFont.TabIndex = 3;
            this.btnFont.UseVisualStyleBackColor = true;
            this.btnFont.Click += new System.EventHandler(this.btnFont_Click);
            // 
            // btnAbout
            // 
            this.btnAbout.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbout.Location = new System.Drawing.Point(75, 168);
            this.btnAbout.Name = "btnAbout";
            this.btnAbout.Size = new System.Drawing.Size(75, 23);
            this.btnAbout.TabIndex = 2;
            this.btnAbout.Text = "About";
            this.btnAbout.UseVisualStyleBackColor = true;
            this.btnAbout.Click += new System.EventHandler(this.btnAbout_Click);
            // 
            // ddScreen
            // 
            this.ddScreen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ddScreen.FormattingEnabled = true;
            this.ddScreen.Location = new System.Drawing.Point(3, 26);
            this.ddScreen.Name = "ddScreen";
            this.ddScreen.Size = new System.Drawing.Size(147, 21);
            this.ddScreen.TabIndex = 1;
            this.ddScreen.DropDown += new System.EventHandler(this.ddScreen_DropDown);
            this.ddScreen.SelectedIndexChanged += new System.EventHandler(this.ddScreen_SelectedIndexChanged);
            // 
            // chkPresenter
            // 
            this.chkPresenter.AutoSize = true;
            this.chkPresenter.Location = new System.Drawing.Point(3, 3);
            this.chkPresenter.Name = "chkPresenter";
            this.chkPresenter.Size = new System.Drawing.Size(100, 17);
            this.chkPresenter.TabIndex = 0;
            this.chkPresenter.Text = "Show presenter";
            this.chkPresenter.UseVisualStyleBackColor = true;
            this.chkPresenter.CheckedChanged += new System.EventHandler(this.chkPresenter_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.pnlView);
            this.panel1.Controls.Add(this.pnlControl);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(471, 833);
            this.panel1.TabIndex = 0;
            // 
            // tmrPlay
            // 
            this.tmrPlay.Tick += new System.EventHandler(this.tmrPlay_Tick);
            // 
            // chkShowCTag
            // 
            this.chkShowCTag.AutoSize = true;
            this.chkShowCTag.Location = new System.Drawing.Point(3, 95);
            this.chkShowCTag.Name = "chkShowCTag";
            this.chkShowCTag.Size = new System.Drawing.Size(107, 17);
            this.chkShowCTag.TabIndex = 5;
            this.chkShowCTag.Text = "Show current tag";
            this.chkShowCTag.UseVisualStyleBackColor = true;
            this.chkShowCTag.CheckedChanged += new System.EventHandler(this.chkShowCTag_CheckedChanged);
            // 
            // ldData
            // 
            this.ldData.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ldData.BackColor = System.Drawing.Color.Transparent;
            this.ldData.Location = new System.Drawing.Point(279, 381);
            this.ldData.Name = "ldData";
            this.ldData.Rate = 100;
            this.ldData.Size = new System.Drawing.Size(64, 64);
            this.ldData.TabIndex = 11;
            this.ldData.Text = "loader1";
            this.ldData.Visible = false;
            // 
            // ldPreview
            // 
            this.ldPreview.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ldPreview.BackColor = System.Drawing.Color.Transparent;
            this.ldPreview.Location = new System.Drawing.Point(198, 50);
            this.ldPreview.Name = "ldPreview";
            this.ldPreview.Rate = 100;
            this.ldPreview.Size = new System.Drawing.Size(64, 64);
            this.ldPreview.TabIndex = 11;
            this.ldPreview.Text = "loader1";
            this.ldPreview.Visible = false;
            // 
            // cnt
            // 
            this.cnt.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.cnt.FillColor = System.Drawing.SystemColors.ControlDark;
            this.cnt.Location = new System.Drawing.Point(293, 3);
            this.cnt.Name = "cnt";
            this.cnt.Progress = 0F;
            this.cnt.Size = new System.Drawing.Size(32, 32);
            this.cnt.TabIndex = 8;
            // 
            // Controller
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1263, 833);
            this.Controls.Add(this.splitContainer2);
            this.Name = "Controller";
            this.Text = "InstaFeed";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.pnlFilter.ResumeLayout(false);
            this.pnlFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nbMax)).EndInit();
            this.pnlView.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.imgNext)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.imgCurrent)).EndInit();
            this.pnlControl.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.nbTime)).EndInit();
            this.pnlData.ResumeLayout(false);
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtTag;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtFrom;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker dtTo;
        private System.Windows.Forms.Panel pnlFilter;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.NumericUpDown nbMax;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ListView lvData;
        private System.Windows.Forms.Panel pnlView;
        private System.Windows.Forms.Panel pnlControl;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.PictureBox imgNext;
        private System.Windows.Forms.PictureBox imgCurrent;
        private System.Windows.Forms.Button btnPrev;
        private System.Windows.Forms.Button btnNext;
        private System.Windows.Forms.Button btnPause;
        private System.Windows.Forms.NumericUpDown nbTime;
        private System.Windows.Forms.ColumnHeader colUser;
        private System.Windows.Forms.CheckBox chkBlackout;
        private System.Windows.Forms.Button btnSetImage;
        private System.Windows.Forms.ColumnHeader colDate;
        private Loader ldPreview;
        private Loader ldData;
        private System.Windows.Forms.Panel pnlData;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Timer tmrPlay;
        private Countdown cnt;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.CheckBox chkPresenter;
        private System.Windows.Forms.ComboBox ddScreen;
        private System.Windows.Forms.Button btnAbout;
        private System.Windows.Forms.Label lblFont;
        private System.Windows.Forms.Button btnFont;
        private System.Windows.Forms.CheckBox chkShowCTag;
    }
}