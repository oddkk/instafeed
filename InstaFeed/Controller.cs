﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstaFeed
{
    public partial class Controller : Form
    {
        ImageList list;
        Presenter presenter;

        private void updateFont()
        {
            presenter.PresenterFont = PresenterFont;
            btnFont.Text = String.Format("{0} {1}pt", PresenterFont.Name, PresenterFont.Size.ToString());
        }

        public Font PresenterFont
        {
            get
            {
                return InstaFeed.Properties.Settings.Default.PresenterFont;
            }
            set
            {
                InstaFeed.Properties.Settings.Default.PresenterFont = value;
                InstaFeed.Properties.Settings.Default.Save();
                updateFont();
            }
        }
        
        public Controller()
        {
            InitializeComponent();
            btnRefresh.BackgroundImage = Recolor.Alpha(InstaFeed.Properties.Resources.refresh27, SystemColors.ControlText);
            btnSetImage.BackgroundImage = Recolor.Alpha(InstaFeed.Properties.Resources.arrow167, SystemColors.ControlText);
            chkBlackout.BackgroundImage = Recolor.Alpha(InstaFeed.Properties.Resources.black170, SystemColors.ControlText);
            btnPlay.BackgroundImage = Recolor.Alpha(InstaFeed.Properties.Resources.media23, SystemColors.ControlText);
            btnPause.BackgroundImage = Recolor.Alpha(InstaFeed.Properties.Resources.pause18, SystemColors.ControlText);
            btnNext.BackgroundImage = Recolor.Alpha(InstaFeed.Properties.Resources.forward11, SystemColors.ControlText);
            btnPrev.BackgroundImage = Recolor.Alpha(InstaFeed.Properties.Resources.media24, SystemColors.ControlText);

            list = new ImageList();
            list.LoadNextImageBegin += list_LoadNextImageBegin;
            list.LoadNextImageComplete += list_LoadNextImageComplete;
            list.ReloadListBegin += list_ReloadListBegin;
            list.ReloadListComplete += list_ReloadListComplete;
            list.CurrentChanged += list_CurrentChanged;

            presenter = new Presenter();
            presenter.FormClosing += presenter_FormClosing;
            LoadScreens();
            if (ddScreen.Items.Count > 1)
                ddScreen.SelectedIndex = 1;
            else ddScreen.SelectedIndex = 0;
            try
            {
                updateFont();
                chkShowCTag.Checked = InstaFeed.Properties.Settings.Default.ShowTopLeftText;
                if (InstaFeed.Properties.Settings.Default.LastFrom > dtFrom.MinDate &&
                    InstaFeed.Properties.Settings.Default.LastFrom < dtFrom.MaxDate)
                    dtFrom.Value = InstaFeed.Properties.Settings.Default.LastFrom;
                if (InstaFeed.Properties.Settings.Default.LastTo > dtTo.MinDate &&
                    InstaFeed.Properties.Settings.Default.LastTo < dtTo.MaxDate)
                    dtTo.Value = InstaFeed.Properties.Settings.Default.LastTo;
                nbMax.Value = InstaFeed.Properties.Settings.Default.LastMax;
                txtTag.Text = InstaFeed.Properties.Settings.Default.LastTag;
                nbTime.Value = (decimal)InstaFeed.Properties.Settings.Default.NextInterval;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                InstaFeed.Properties.Settings.Default.Save();
            }
        }

        void presenter_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            presenter.Hide();
            chkPresenter.Checked = false;
        }

        bool isHighlighting = false;
        void list_CurrentChanged(object sender, EventArgs e)
        {
            imgCurrent.Image = list.current;
            if (lvData.Items.Count <= list.nextIndex()) return;
            lvData.Items[list.nextIndex()].EnsureVisible();
            isHighlighting = true;
            for (int i = 0; i < lvData.Items.Count; i++)
                lvData.Items[i].Selected = false;
            lvData.Items[list.nextIndex()].Selected = true;
            isHighlighting = false;
            presenter.Change(list.current,list.GetCurrentText());
        }

        void list_ReloadListComplete(object sender, EventArgs e)
        {
            ldData.Visible = false;
            for (int i = 0; i < list.images.Count; i++)
            {
                lvData.Items.Add(new ListViewItem(new string[] { list.images[i].User, list.images[i].Date.ToString() }));
            }
        }

        void list_ReloadListBegin(object sender, EventArgs e)
        {
            ldData.Visible = true;
            lvData.Items.Clear();
        }

        void list_LoadNextImageComplete(object sender, EventArgs e)
        {
            ldPreview.Visible = false;
            imgNext.Image = list.next;
        }

        void list_LoadNextImageBegin(object sender, EventArgs e)
        {
            ldPreview.Visible = true;
            imgNext.Image = null;
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            list.SetTag(txtTag.Text, dtFrom.Value, dtTo.Value, (int)nbMax.Value);
            if (chkShowCTag.Checked)
            {
                presenter.TopLeftText ="#" + txtTag.Text;
            }
        }

        private void lvData_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (!isHighlighting)
            {
                if (lvData.SelectedItems.Count > 0)
                    list.SetNext(lvData.SelectedItems[0].Index);
            }
        }

        private void btnSetImage_Click(object sender, EventArgs e)
        {
            list.Advance();
        }

        private void chkBlackout_CheckedChanged(object sender, EventArgs e)
        {
            list.BlackOut(chkBlackout.Checked);
        }

        private void txtTag_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Enter)
            {
                e.Handled = true;
                btnRefresh_Click(sender, e);
            }
        }

        int time = 0;
        private void tmrPlay_Tick(object sender, EventArgs e)
        {
            time++;
            if (time > (int)(nbTime.Value * 10))
            {
                time = 0;
                list.Advance();
            }
            cnt.Progress = (float)time / (float)(nbTime.Value * 10);
        }

        private void nbTime_ValueChanged(object sender, EventArgs e)
        {
            InstaFeed.Properties.Settings.Default.NextInterval = (double)nbTime.Value;
            InstaFeed.Properties.Settings.Default.Save();
        }

        private void btnPlay_Click(object sender, EventArgs e)
        {
            list.Advance();
            tmrPlay.Start();
        }

        private void btnPause_Click(object sender, EventArgs e)
        {
            time = 0;
            cnt.Progress = 0;
            tmrPlay.Stop();
        }

        private void LoadScreens()
        {
            ddScreen.Items.Clear();
            for (int i = 0; i < Screen.AllScreens.Length; i++)
            {
                ddScreen.Items.Add((i + 1).ToString() + " - " + Screen.AllScreens[i].DeviceName);
            }
        }

        private void ddScreen_SelectedIndexChanged(object sender, EventArgs e)
        {
            presenter.SetScreen(ddScreen.SelectedIndex);
        }

        private void chkPresenter_CheckedChanged(object sender, EventArgs e)
        {
            if (chkPresenter.Checked)
            {
                presenter.Show();
            }
            else
            {
                presenter.Hide();
            }
        }

        private void ddScreen_DropDown(object sender, EventArgs e)
        {
            LoadScreens();
        }

        private void btnAbout_Click(object sender, EventArgs e)
        {
            About a = new About();
            a.ShowDialog();
        }

        private void btnFont_Click(object sender, EventArgs e)
        {
            FontDialog fd = new FontDialog();

            fd.Font = PresenterFont;
            if (fd.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                PresenterFont = fd.Font;
            }
        }

        private void chkShowCTag_CheckedChanged(object sender, EventArgs e)
        {
            InstaFeed.Properties.Settings.Default.ShowTopLeftText = chkShowCTag.Checked;
            InstaFeed.Properties.Settings.Default.Save();
            if (chkShowCTag.Checked)
            {
                presenter.TopLeftText = "#" + txtTag.Text;
            }
            else
            {
                presenter.TopLeftText = "";
            }
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            list.SetNext(list.nextIndex() + 1 >= list.images.Count ? 0 : list.nextIndex() + 1);
        }

        private void btnPrev_Click(object sender, EventArgs e)
        {
            list.SetNext(list.prevIndex());
        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            InstaFeed.Properties.Settings.Default.LastFrom = dtFrom.Value;
            InstaFeed.Properties.Settings.Default.Save();
        }

        private void dtTo_ValueChanged(object sender, EventArgs e)
        {
            InstaFeed.Properties.Settings.Default.LastTo= dtTo.Value;
            InstaFeed.Properties.Settings.Default.Save();
        }

        private void txtTag_TextChanged(object sender, EventArgs e)
        {
            InstaFeed.Properties.Settings.Default.LastTag = txtTag.Text;
            InstaFeed.Properties.Settings.Default.Save();
        }

        private void nbMax_ValueChanged(object sender, EventArgs e)
        {

            InstaFeed.Properties.Settings.Default.LastMax = (int)nbMax.Value;
            InstaFeed.Properties.Settings.Default.Save();
        }
    }
}
