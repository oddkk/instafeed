﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace InstaFeed
{
    public class Countdown : Control
    {
        public Countdown()
        {
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
            FillColor = SystemColors.ControlDark;
        }

        private float _progress;
        public float Progress
        {
            get
            {
                return _progress;
            }
            set
            {
                _progress = value;
                this.Invalidate();
            }
        }
        public Color FillColor { get; set; }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
            e.Graphics.FillPie(new SolidBrush(this.FillColor), new Rectangle(0, 0, Width, Height),   0.0f, 360.0f);
            e.Graphics.FillPie(new SolidBrush(this.ForeColor), new Rectangle(0, 0, Width, Height), -90.0f, Progress * 360.0f);//2.0f * (float)Math.PI);
        }
    }
}
