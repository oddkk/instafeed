﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Net;
using System.Runtime.Serialization.Json;

namespace InstaFeed
{
    public class InstagramImage
    {
        public string User;
        public string Message;
        public DateTime Date;
        public string ImageUrl;
    }

    public class Instagram
    {
        public class Tags
        {
            public _data[] data { get; set; }
            public _pagination pagination { get; set; }

            public class _data
            {
                public string type { get; set; }
                public _caption caption { get; set; }
                public _images images { get; set; }
                

                public class _caption
                {
                    public string created_time { get; set; }
                    public string text { get; set; }
                    public _user from { get; set; }

                }
                public class _images
                {
                    public _image low_resolution { get; set; }
                    public _image thumbnail { get; set; }
                    public _image standard_resolution { get; set; }
                }
            }

            public class _user
            {
                public string id { get; set; }
                public string username { get; set; }
            }
            public class _image
            {
                public string url { get; set; }
                public int width { get; set; }
                public int height { get; set; }
            }

            public class _pagination
            {
                public string next_url { get; set; }
                public string next_max_id { get; set; }
            }
        }

        private string clientId;

        public Instagram(string clientid)
        {
            this.clientId = clientid;
        }

        public InstagramImage[] GetImages(string tag,DateTime minDate,DateTime maxDate, int maxImgs, string minId = "")
        {
            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create("https://api.instagram.com/v1/tags/" + tag + "/media/recent?client_id=" + clientId + (minId != "" ? "&max_id=" + minId : ""));
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(Tags));

                Tags res = (Tags)serializer.ReadObject(response.GetResponseStream());
                if (res == null || res.data == null || res.pagination == null) return new InstagramImage[0];
                List<InstagramImage> imgs = new List<InstagramImage>();

                for (int i = 0; i < Math.Min(maxImgs,res.data.Length); i++)
                {
                    if (res.data[i].type != "image") continue;
                    InstagramImage img = new InstagramImage();

                    if (res.data[i] == null) continue;
                    if (res.data[i].caption != null)
                    {
                        img.Message = res.data[i].caption.text;
                        img.Date = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc).AddSeconds(long.Parse(res.data[i].caption.created_time)).ToLocalTime(); // Convert from UNIX timestamp
                        if (res.data[i].caption.from != null)
                            img.User = res.data[i].caption.from.username;
                    }
                    if (res.data[i].images != null && res.data[i].images.standard_resolution != null)
                        img.ImageUrl = res.data[i].images.standard_resolution.url;
                    if (img.Date > minDate && img.Date < maxDate)
                    {
                        imgs.Add(img);
                    }
                }

                if (imgs.Count < maxImgs && imgs.Count > 0 && imgs[imgs.Count - 1].Date < maxDate)
                {
                    imgs.AddRange(GetImages(tag,minDate,maxDate,maxImgs - imgs.Count,res.pagination.next_max_id));
                }

                return imgs.ToArray();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
