﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
//using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace InstaFeed
{
    public partial class Presenter : Form
    {
        private Bitmap current = null;
        private Bitmap prev = null;
        private string currentText = "";
        private int fadeTime;
        private int cScreen = 0;
        private ColorMatrix cmPrev;
        private ColorMatrix cmCurrent;
        private ImageAttributes atPrev;
        private ImageAttributes atCurrent;
        private int TotFadeTime = 1000;

        public Font PresenterFont;
        public string TopLeftText = "";

        public Presenter()
        {
            InitializeComponent();
            SetStyle(ControlStyles.AllPaintingInWmPaint | ControlStyles.UserPaint | ControlStyles.OptimizedDoubleBuffer, true);
            if (Screen.AllScreens.Length > 1)
                cScreen = 1;
            SetScreen(cScreen);
            cmPrev = new ColorMatrix();
            atPrev = new ImageAttributes();
            atPrev.SetColorMatrix(cmPrev);

            cmCurrent = new ColorMatrix();
            atCurrent = new ImageAttributes();
            atCurrent.SetColorMatrix(cmCurrent);
        }
        DateTime LastDraw = DateTime.Now;
        private void Presenter_Paint(object sender, PaintEventArgs e)
        {
             int dtime = (int)(DateTime.Now - LastDraw).TotalMilliseconds;
             LastDraw = DateTime.Now;

             fadeTime -= dtime;
             if (fadeTime <= 0)
             {
                 fadeTime = 0;
                 if (prev != null)
                     prev.Dispose();
                 prev = null;
             }

             cmCurrent.Matrix33 = 1.0f - (float)fadeTime / (float)TotFadeTime;
             atCurrent.SetColorMatrix(cmCurrent);

            Rectangle imageRect = Rectangle.Empty;
            Point imageCenter = new Point(this.Width / 2, (int)((float)this.Height * (1 - 0.618033)));

             if (prev != null)
             {
                 imageRect = new Rectangle(imageCenter.X - prev.Width / 2, imageCenter.Y - prev.Height / 2, prev.Width, prev.Height);
                 e.Graphics.DrawImage(prev, imageRect, 0, 0, prev.Width, prev.Height, GraphicsUnit.Pixel, atPrev);
             }
             if (current != null)
             {
                 imageRect = new Rectangle(imageCenter.X - current.Width / 2, imageCenter.Y - current.Height / 2, current.Width, current.Height);
                 e.Graphics.DrawImage(current, imageRect, 0, 0, current.Width, current.Height, GraphicsUnit.Pixel, atCurrent);
             }
             if (!imageRect.IsEmpty)
                 e.Graphics.DrawString(currentText, PresenterFont, Brushes.White, new RectangleF(imageRect.X, imageRect.Bottom,imageRect.Width,10000));

             e.Graphics.DrawString(TopLeftText, PresenterFont, Brushes.White, new PointF(0.0f,0.0f));
        }

        public void SetScreen(int i)
        {
            cScreen = i;
            Point location = Screen.AllScreens[i].Bounds.Location;
            Size size = Screen.AllScreens[i].Bounds.Size;

            this.SetDesktopBounds(location.X, location.Y, size.Width, size.Height);
        }

        public void Change(Bitmap next, string text)
        {
            Bitmap tmp = this.prev;
            this.prev = current;
            if (next != null)
                this.current = (Bitmap)next.Clone();
            else
            {
                this.current = new Bitmap(prev.Width, prev.Height);
                using (Graphics gr = Graphics.FromImage(current))
                {
                    gr.Clear(Color.Black);
                }
            }
            currentText = text;
            fadeTime = TotFadeTime;

            if (tmp != null)
                tmp.Dispose();
            tmp = null;
        }

        private void tmrDraw_Tick(object sender, EventArgs e)
        {
            Invalidate();
        }

        private void Presenter_Shown(object sender, EventArgs e)
        {
            SetScreen(cScreen);
        }
    }
}
